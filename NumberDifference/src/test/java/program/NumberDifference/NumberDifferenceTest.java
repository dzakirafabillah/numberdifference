package program.NumberDifference;

import java.util.ArrayList;
import java.util.Arrays;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import program.CalcNUmberDifference;

public class NumberDifferenceTest {
	private CalcNUmberDifference sut;
	@Before
	public void setUp() throws Exception {
		sut = new CalcNUmberDifference();
	}

        public void testjumlahDeretBil(int jumlahDeretBil, boolean expectedValidateNResult) {
		// (1) setup (arrange, build)
		boolean actualValidateNResult;
		// (2) exercise (act, operate)
		actualValidateNResult = sut.validateRangeInputMaxDeret(jumlahDeretBil);
		// (3) verify (assert, check)
		assertEquals(expectedValidateNResult, actualValidateNResult);
	}
        
        public void testDifferenceResult( ArrayList<Integer> numberSeries, int expectedDiffResult) {
		// (1) setup (arrange, build)
                int actualDiffResult;
		// (2) exercise (act, operate)
		actualDiffResult = sut.numberDiffirenceProcess(numberSeries);
		// (3) verify (assert, check)
                assertEquals(expectedDiffResult, actualDiffResult);
	}
        
        public void testGroupingResult(int actualDiffResult, String expectedGroupingResult) {
		// (1) setup (arrange, build)
                String actualGroupingResult;
		// (2) exercise (act, operate)
                actualGroupingResult=sut.groupingDifference(actualDiffResult);
		// (3) verify (assert, check)
                assertEquals(expectedGroupingResult, actualGroupingResult);
	}
        
        @Test
        public void case1_batasBawahGroup3_1loop(){
            /*Initialize test data*/
            int jumlahDeretBil = 2;
            
            ArrayList<Integer> numberSeries = new ArrayList<>(Arrays.asList(100, 49));
       
            int expectedDiffResult = 51;
            int actualDiffResult = sut.numberDiffirenceProcess(numberSeries);;
            String expectedGroupingResult = "Difference : 51\n" +
                                            "Group 3, Large Difference";
            
            /*Test*/
            testjumlahDeretBil(jumlahDeretBil, true);
            testDifferenceResult(numberSeries, expectedDiffResult);
            testGroupingResult(actualDiffResult, expectedGroupingResult);
        }
        @Test
        public void case2_rangeGroup3_1loop(){
            /*Initialize test data*/
            int jumlahDeretBil = 2;
            
            ArrayList<Integer> numberSeries = new ArrayList<>(Arrays.asList(100, 30));
            
            int expectedDiffResult = 70;
            int actualDiffResult = sut.numberDiffirenceProcess(numberSeries);;
            String expectedGroupingResult = "Difference : 70\n" +
                                            "Group 3, Large Difference";
            
            /*Test*/
            testjumlahDeretBil(jumlahDeretBil, true);
            testDifferenceResult(numberSeries, expectedDiffResult);
            testGroupingResult(actualDiffResult, expectedGroupingResult);
        }
        
        @Test
        public void case3_rangeGroup3_2loop(){
            /*Initialize test data*/
            int jumlahDeretBil = 3;
            
            ArrayList<Integer> numberSeries = new ArrayList<>(Arrays.asList(300, 200, 100));
            
            int expectedDiffResult = 200;
            int actualDiffResult = sut.numberDiffirenceProcess(numberSeries);;
            String expectedGroupingResult = "Difference : 200\n" +
                                            "Group 3, Large Difference";
            
            /*Test*/
            testjumlahDeretBil(jumlahDeretBil, true);
            testDifferenceResult(numberSeries, expectedDiffResult);
            testGroupingResult(actualDiffResult, expectedGroupingResult);
        }
        
        @Test
        public void case4_rangeGroup3_5loop(){
           /*Initialize test data*/
            int jumlahDeretBil = 6;
            
            ArrayList<Integer> numberSeries = new ArrayList<>(Arrays.asList(300, 250, 200, 150, 100, 50));
            
            int expectedDiffResult = 250;
            int actualDiffResult = sut.numberDiffirenceProcess(numberSeries);;
            String expectedGroupingResult = "Difference : 250\n" +
                                            "Group 3, Large Difference";
            
            /*Test*/
            testjumlahDeretBil(jumlahDeretBil, true);
            testDifferenceResult(numberSeries, expectedDiffResult);
            testGroupingResult(actualDiffResult, expectedGroupingResult);  
        }
        
        @Test
        public void case5_rangeGroup3_8loop(){
            /*Initialize test data*/
            int jumlahDeretBil = 9;
            
            ArrayList<Integer> numberSeries = new ArrayList<>(Arrays.asList(200, 190, 180,170, 160, 150, 140, 130, 120));
            
            int expectedDiffResult = 80;
            int actualDiffResult = sut.numberDiffirenceProcess(numberSeries);;
            String expectedGroupingResult = "Difference : 80\n" +
                                            "Group 3, Large Difference";
            
            /*Test*/
            testjumlahDeretBil(jumlahDeretBil, true);
            testDifferenceResult(numberSeries, expectedDiffResult);
            testGroupingResult(actualDiffResult, expectedGroupingResult);
        }

        
        @Test
        public void case6_rangeGroup3_9loop(){
            /*Initialize test data*/
            int jumlahDeretBil = 10;
            
            ArrayList<Integer> numberSeries = new ArrayList<>(Arrays.asList(200, 190, 180,170, 160, 150, 140, 130, 120, 110));
            
            int expectedDiffResult = 90;
            int actualDiffResult = sut.numberDiffirenceProcess(numberSeries);;
            String expectedGroupingResult = "Difference : 90\n" +
                                            "Group 3, Large Difference";
            
            /*Test*/
            testjumlahDeretBil(jumlahDeretBil, true);
            testDifferenceResult(numberSeries, expectedDiffResult);
            testGroupingResult(actualDiffResult, expectedGroupingResult);
        }
        
        @Test
        public void case7_batasBawahGroup2_1loop(){
            /*Initialize test data*/
            int jumlahDeretBil = 2;
            
            ArrayList<Integer> numberSeries = new ArrayList<>(Arrays.asList(9, 20));
            
            int expectedDiffResult = 11;
            int actualDiffResult = sut.numberDiffirenceProcess(numberSeries);;
            String expectedGroupingResult = "Difference : 11\n" +
                                            "Group 2, Medium Difference";
            
            /*Test*/
            testjumlahDeretBil(jumlahDeretBil, true);
            testDifferenceResult(numberSeries, expectedDiffResult);
            testGroupingResult(actualDiffResult, expectedGroupingResult);
        }
        
        
        @Test
        public void case8_rangeGroup2_1loop(){
             /*Initialize test data*/
            int jumlahDeretBil = 2;
            
            ArrayList<Integer> numberSeries = new ArrayList<>(Arrays.asList(20, 50));
            
            int expectedDiffResult = 30;
            int actualDiffResult = sut.numberDiffirenceProcess(numberSeries);;
            String expectedGroupingResult = "Difference : 30\n" +
                                            "Group 2, Medium Difference";
            
            /*Test*/
            testjumlahDeretBil(jumlahDeretBil, true);
            testDifferenceResult(numberSeries, expectedDiffResult);
            testGroupingResult(actualDiffResult, expectedGroupingResult);
        }
        
        @Test
        public void case9_batasAtasGroup2_1loop(){
            /*Initialize test data*/
            int jumlahDeretBil = 2;
            
            ArrayList<Integer> numberSeries = new ArrayList<>(Arrays.asList(50, 100));
            
            int expectedDiffResult = 50;
            int actualDiffResult = sut.numberDiffirenceProcess(numberSeries);;
            String expectedGroupingResult = "Difference : 50\n" +
                                            "Group 2, Medium Difference";
            
            /*Test*/
            testjumlahDeretBil(jumlahDeretBil, true);
            testDifferenceResult(numberSeries, expectedDiffResult);
            testGroupingResult(actualDiffResult, expectedGroupingResult);
        }
        
        @Test
        public void case10_batasBawahGroup1_1loop(){
            /*Initialize test data*/
            int jumlahDeretBil = 2;
            
            ArrayList<Integer> numberSeries = new ArrayList<>(Arrays.asList(2, 1));
            
            int expectedDiffResult = 1;
            int actualDiffResult = sut.numberDiffirenceProcess(numberSeries);;
            String expectedGroupingResult = "Difference : 1\n" +
                                            "Group 1, Small Difference";
            
            /*Test*/
            testjumlahDeretBil(jumlahDeretBil, true);
            testDifferenceResult(numberSeries, expectedDiffResult);
            testGroupingResult(actualDiffResult, expectedGroupingResult);
        }
        
        
        @Test
        public void case11_rangeGroup1_1loop(){
            /*Initialize test data*/
            int jumlahDeretBil = 2;
            
            ArrayList<Integer> numberSeries = new ArrayList<>(Arrays.asList(10, 5));
            
            int expectedDiffResult = 5;
            int actualDiffResult = sut.numberDiffirenceProcess(numberSeries);;
            String expectedGroupingResult = "Difference : 5\n" +
                                            "Group 1, Small Difference";
            
            /*Test*/
            testjumlahDeretBil(jumlahDeretBil, true);
            testDifferenceResult(numberSeries, expectedDiffResult);
            testGroupingResult(actualDiffResult, expectedGroupingResult);
        }
        
        @Test
        public void case12_batasAtasGroup1_1loop(){
            /*Initialize test data*/
            int jumlahDeretBil = 2;
            
            ArrayList<Integer> numberSeries = new ArrayList<>(Arrays.asList(20, 10));
            
            int expectedDiffResult = 10;
            int actualDiffResult = sut.numberDiffirenceProcess(numberSeries);;
            String expectedGroupingResult = "Difference : 10\n" +
                                            "Group 1, Small Difference";
            
            /*Test*/
            testjumlahDeretBil(jumlahDeretBil, true);
            testDifferenceResult(numberSeries, expectedDiffResult);
            testGroupingResult(actualDiffResult, expectedGroupingResult);
        }
        
        @Test
        public void case13_nonGroup_1loop(){
            /*Initialize test data*/
            int jumlahDeretBil = 2;
            
            ArrayList<Integer> numberSeries = new ArrayList<>(Arrays.asList(1, 1));
            
            int expectedDiffResult = 0;
            int actualDiffResult = sut.numberDiffirenceProcess(numberSeries);;
            String expectedGroupingResult = "Difference : 0\n" +
                                            "Non group, There's no difference";
            
            /*Test*/
            testjumlahDeretBil(jumlahDeretBil, true);
            testDifferenceResult(numberSeries, expectedDiffResult);
            testGroupingResult(actualDiffResult, expectedGroupingResult);
        }
        
        @Test
        public void case14_nonGroup_0loop(){
           /*Initialize test data*/
            int jumlahDeretBil = 1;
            
            ArrayList<Integer> numberSeries = new ArrayList<>(Arrays.asList(1));
            
            int expectedDiffResult = 0;
            int actualDiffResult = sut.numberDiffirenceProcess(numberSeries);;
            String expectedGroupingResult = "Difference : 0\n" +
                                            "Non group, There's no difference";
            
            /*Test*/
            testjumlahDeretBil(jumlahDeretBil, true);
            testDifferenceResult(numberSeries, expectedDiffResult);
            testGroupingResult(actualDiffResult, expectedGroupingResult); 
        } 
        
        @Test
        public void case15_N_LebihDariBatasBawah(){
            /*Initialize test data*/
            int jumlahDeretBil = 0;
            testjumlahDeretBil(jumlahDeretBil, false);
        } 
        @Test
        public void case16_N_LebihDariBatasAtas(){
            /*Initialize test data*/
            int jumlahDeretBil = 11;
            testjumlahDeretBil(jumlahDeretBil, false);
        } 
}
